﻿using Documents;
using System;
using System.Collections.Generic;
using System.IO;

namespace DZ7
{
    class Program
    {
        public static void Main(string[] args)
        {
            string folderPath = @"C:\temp";
            Console.WriteLine($"По умолчанию путь {folderPath}, нажмите Enter если хоитите оставить путь по умолчанию");
            Console.Write("Введите путь до папки с документами : ");
            try
            {
                string InfolderPath = Console.ReadLine();
                if (!string.IsNullOrEmpty(InfolderPath))
                    folderPath = InfolderPath;
                if (!Directory.Exists(folderPath))
                    throw new DirectoryNotFoundException($"Введенный путь \"{folderPath}\" не найден.");
                Console.Write("Введите таймаут ожидания документов в секундах: ");
                int waitingInterval;
                bool res = int.TryParse(Console.ReadLine(), out waitingInterval);

                List<string> listFiles = new() { "Заявление.txt", "Фото.jpg", "Паспорт.jpg" };
                DocumentsReceiver receiver = new DocumentsReceiver(listFiles);
                receiver.DocumentsReady += Documents_Ready;
                receiver.TimedOut += Documents_TimedOut;
                receiver.Start(folderPath, waitingInterval*1000);
                Console.ReadKey();
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine($"ErrorPath - {e.Message}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error - {e.Message}");
            }
        }

        private static void Documents_Ready(object obj, FileSystemEventArgs e)
        {
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:
                    Console.WriteLine($"Создан файл - {e.Name}");
                    break;
                case WatcherChangeTypes.Deleted:
                    Console.WriteLine($"Удален файл - {e.Name}");
                    break;
            }
            if (((DocumentsReceiver)obj).FilesAllLoaded)
                Console.WriteLine("Все файлы загружены!"); ;;

        }

        private static void Documents_TimedOut()
        {
            Console.WriteLine("Время для загрузки файлов вышло!");
        }
    }
}
